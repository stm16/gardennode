"""
hello_flask: First Python-Flask webapp
"""

from flask import Flask, render_template, request
from helpers import *
app = Flask(__name__)  





# Our pages... 
@app.route('/')  
def index():
    # Homepage shows all sensors and buttons
    s = '<script src="/static/jq331.js"></script>\n'
    return s + 'Hello, world!' + a_button("On/Off") + my_js_eventhandler()
    
@app.route('/main')  
def main():
    return render_template('base.html',buttons=[1,2,3])
    
# And some endpoints meant for ajax. Use separate calls
# for turning on and off, to avoid the confusion of
# 'toggling'.
#
# Also, I want to send the GPIO command, and then query
# the state so that I reply with the true state, not just
# the one that I asked for. 

@app.route('/test')
def x():
    return web_from_config()

@app.route('/turnon/<buttonnum>')
def turnon(buttonnum):
    return set_pin(buttonnum,1)

@app.route('/turnoff/<buttonnum>')
def turnoff(buttonnum):
    return set_pin(buttonnum,0)

@app.route('/get/<buttonnum>')
def get(buttonnum):
    return check_pin(buttonnum)

@app.route('/check_all')
def check_all():
    return check_all_pins()

@app.errorhandler(404)
def not_found(error):
    return h1('Sorry, this page was not found.')

if __name__ == '__main__':  # Script executed directly?
    app.run(debug=True)  # Launch built-in web server and run this Flask webapp
