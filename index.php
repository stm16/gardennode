<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Chickencoop Garden</title>
<!-- v2: react classes
   - onoffbtn
   - dashboard digits
   - small graph, 1 day, 1 mo, 1 yr
   everything else is labels or decoration
-->
</head>

<body onload="startup()">
<?php


?>
<style> 
body { font-family: Arial, Helvetica, sans-serif; }
span, .onoff { cursor: pointer; } #lhs {float: left; margin-right: 2em; }
.ndy { color: #999; }
p, .onoff { vertical-align: middle; }
</style>
<h1>Home Control</h1>
<div id="lhs">
<img src="img/icon_drop.png" width="75" height="75" /><br />
<p class="ndy" id="humidityGreenhouse">Greenhouse humidity: <span>20</span>%</p>
<p class="ndy" id="moistureBerm">Berm moisture: <span>25</span>%</p>
<p class="ndy" id="moistureContainer1">Pot moisture: <span>30</span>%</p>
<p>&nbsp;</p>
<p><img src="img/icon_bucket.png" width="75" height="75" /></p>
<p>Drippers: <img id="dripBtn" class="onoff" src="img/icon_off.png" width="75" height="24" alt="OFF" /></p>
<p>Sprayers: <img id="sprayBtn" class="onoff" src="img/icon_off.png" width="75" height="24" alt="ON" /></p>
<p>Soakers: <img id="soakBtn" class="onoff" src="img/icon_off.png" width="75" height="24" alt="ON" /></p>
<p>&nbsp;</p>
<p><img src="img/icon_bucket.png" width="75" height="75" /></p>
<p>Hydro Pump: <img id="hydro1Btn" class="onoff" src="img/icon_off.png" width="75" height="24" alt="OFF" /></p>
<p>Hydro Heater: <img id="hydro2Btn" class="onoff" src="img/icon_off.png" width="75" height="24" alt="ON" /></p>
</div>
<div id="rhs">
<img src="img/icon_battery.png" width="75" height="75" /><br />
<p class="ndy" id="humidityBathroom">Bathroom Humidity: <span>10</span>%</p>
<p class="ndy">Bathroom Fan: <img id="bathFanBtn" class="onoff" src="img/icon_on.png" width="75" height="24" alt="ON" /></p>
<p class="ndy" id="voltageSolar">Solar Panel: <span>18.1</span> Volts</p>
<p class="ndy" id="voltageBattery">Battery: <span>78</span>% CHARGED</p>
<p><img src="img/icon_temp.png" width="75" height="75" /></p>
<p class="ndy" id="tempInside">Inside Temperature: <span>100</span>&deg; F</p>
<p class="ndy" id="tempUpperGreenhouse">Greenhouse (upper) Temp: <span>120</span>&deg; F</p>
<p class="ndy" id="tempLowerGreenhouse">Greenhouse (ground) Temp: <span>102</span>&deg; F</p>
</div>
<p>&nbsp;</p>


<script>

function update_all() {
	var request = new XMLHttpRequest();
	request.open("GET","gpio.php?startup");
	request.send(null);
	request.onreadystatechange = function() {
		if (request.readyState==4 && request.status==200){
			var pinvals = request.responseText.split(",");
			pinvals.forEach( setButtonOrSensor ); 
		}
	}
}



function startup() {
	update_all();
	setInterval(update_all,5000);
}



// this function sends and receives the pin's status
function change_pin (pin, status, id) {
	//this is the http request
	var request = new XMLHttpRequest();
	console.log("Sending GET gpio.php?pin=" + pin + "&status=" + status);
	request.open( "GET" , "gpio.php?pin=" + pin + "&status=" + status );
	request.send(null);
	//receiving information
	request.onreadystatechange = function () {
		
		if (request.readyState == 4 && request.status == 200) {
			console.log("returned "+request.responseText);
			var responsecode = parseInt(request.responseText);
			if (responsecode==1) {
				changeToOnOff(id,1);
			} else if (responsecode==0) {
				changeToOnOff(id,0);
			} else {
				console.log("possibly wrong: returned "+request.status+" "+request.responseText+"in change_pin");
			}
			return responsecode;
		}
	//test if fail
		else if (request.readyState == 4 && request.status == 500) {
			console.log ("server didn't return what i expected on change_pin.");
			return ("fail");
		}
	//else 
		else { return ("fail"); }
	}
}

function addSensorListener(id) {
	console.log('adding listener to: '+id);
	var elem = document.getElementById(id);
	elem.addEventListener("click", function () { 
			//console.log('listener called');
			//this is the http request
			var request = new XMLHttpRequest();
			console.log("Sending");
			request.open( "GET" , "gpio.php?pin=" + id );
			request.send(null);
			//receiving information
			request.onreadystatechange = function () {
				if (request.readyState == 4 && request.status == 200) {
					setSensorValue(id,request.responseText);
				}
			//test if fail
				else if (request.readyState == 4 && request.status == 500) {
					console.log("server error listenting for sensor val");
					return ("fail");
				}
			//else 
				else { return ("fail"); }
			}
		
		} );
}

function setSensorValue(id,val) {
	//console.log(id);
	var elem = document.getElementById(id);
	var disp = elem.children[0]; // a span inside a p tag.
	disp.textContent = val;
}

function changeToLoading(id) {
	var elem = document.getElementById(id);
	elem.alt = "LOAD"
	elem.width = 18;
	elem.height = 18;
	// elem.src = "img/spiffygif_18x18.gif"; 
}
function changeToOnOff(id,onoff) {
	var msg = "";
	if (onoff==0) { msg = "OFF"; }
	else { msg = "ON"; }
	//console.log("UI: changing "+id+" to "+onoff);
	var elem = document.getElementById(id);
	elem.alt = msg
	elem.width = 75;
	elem.height = 24;
	if (msg=="ON") { elem.src = "img/icon_on.png"; }
	else { elem.src="img/icon_off.png"; }
}


function setButtonOrSensor(pv) {
	// given pv: looks like "soakBtn=0"   or humidityBathroom=75
	// pin = value
	// update the right html to reflect that pin's value
	if (pv) {
	console.log("UI: setting "+pv);
	var kv = pv.split("=");
	if (kv[0].match("Btn")) { changeToOnOff(kv[0],kv[1]); }
	else { setSensorValue(kv[0],kv[1]); }
	}
	
}

function addButtonListener(id) {
	//console.log('adding listener to: '+id);
	var elem = document.getElementById(id);
	//console.log('it is: '+elem);
	elem.addEventListener("click", function () { 
		var state = elem.alt;
		//console.log(state);
		changeToLoading(id);
		//console.log('listener called');
		// button is off
		if ( state == "OFF" ) {
			var new_status = change_pin ( id, 1, id);
		}
		// button is on
		else if ( state == "ON" ) {
			//use the function
			var new_status = change_pin ( id, 0,id);
		} else {
			console.log("trying to click btn in an unknown state.");
		}
		return 0;
	} );
}

/* Get all the ids used in the html above. This is a lazy interface.
   For pin changes, modify: 
	-the id above, in the UI
	-and then again in gpio.php, 
   where it will talk to the raspi. Make them match or stuff will break.
   
   id to actual pin # translation happens in gpio.php
*/

var pinNames = new Array();

var ppp = document.getElementsByTagName('p');
var len = ppp.length;
var str = "These p tags have ids:\n\n";
for (var i=0; i<len; i++) {
  if (ppp[i].id != 'undefined' && ppp[i].id != '') { 
  	pinNames[ppp[i].id] = "sensor"; 
	console.log( "SENSOR: " + ppp[i].id + "\n");
  }
}

var iii = document.getElementsByTagName('img');
var len = iii.length;
for (var i=0; i<len; i++) {
  if (iii[i].id != 'undefined' && iii[i].id != '') { 
  	pinNames[iii[i].id] = "button"; 
	console.log( "BUTTON: " + iii[i].id + "\n");
  }
}


for (var key in pinNames) {
	if (pinNames[key] == 'button') {
		addButtonListener(key);
		console.log ("added listener to " + key);
	}
	if (pinNames[key] == 'sensor') {
		addSensorListener(key);
	}
}



</script>

</body>
</html>
