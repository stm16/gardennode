#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import datetime
import os

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

pinlist = [6,26,19,13,22,27,17]
for p in pinlist:
    GPIO.setup(p,GPIO.OUT)
    GPIO.output(p,1)

for x in range(100):
    for p in pinlist:
        GPIO.output(p,0)
	time.sleep(0.25)
	GPIO.output(p,1)

