#!/usr/bin/python
import RPi.GPIO as GPIO
import dht11
import time
import datetime

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

# read data using pin 14
instance = dht11.DHT11(pin=14)

fakelist = ['a']
for f in fakelist:
    good = 0
    while not good: 
        result = instance.read()
        good = result.is_valid()
    print datetime.datetime.now().strftime("%c")
    r = result.temperature
    t = ((r * 9.0) / 5) + 32
    print("Temperature: " + str(t) + " F")
    print("Humidity: %d %%" % result.humidity)
