﻿<?php

// I do all the talking to the pins. 

// my raspi wiring layout. each key 
// ndy - not done yet

$myPins = array(
    "dripBtn"				=> "5",					// Change an output on the pi.
    "sprayBtn" 				=> "2",					// these correspond to an IMG tag with
	"soakBtn" 				=> "3",					// id of the given key.
	"bathFanBtn" 			=> "4",		// ndy
	"hydro1Btn" => "6",
	"hydro2Btn" => "7",
	"tempInside" 			=> "8",					// Read a sensor on the pi.
	"tempUpperGreenhouse" 	=> "9",		// ndy		// these correspond to a P tag with
	"tempLowerGreenhouse" 	=> "10",				// id of the given key.
	"humidityGreenhouse" 	=> "11",
	"moistureBerm" 			=> "12",	// ndy
	"moistureContainer1" 	=> "13",
	"humidityBathroom" 		=> "14",	// ndy
	"voltageSolar" 			=> "15",	// ndy
	"voltageBattery" 		=> "16",	// ndy
);

$lowestSensorPin = 8;



if (isset ($_GET["startup"])) { 
	$all = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
	$ret = "";
	foreach ($all as $A) { 
		unset ($status);
		$k = array_keys($myPins,$A);
		if ($k) {
			exec("gpio read " . $A,$status);
			$ret .= $k[0] . "=".$status[0].",";
		}
	} 
	echo $ret;
	exit();
}

//Getting and using values
if (isset ($_GET["pin"]) ) {
	$pin = strip_tags($_GET["pin"]);
	$status = strip_tags($_GET["status"]);
	//echo "pin is " . $pin;
	
	
	//Testing if values are legit
	if ( isset($myPins[$pin]) ) {
		//echo "good so far";
		$p = $myPins[$pin];
		
		
		if ($myPins[$pin] < $lowestSensorPin) {				// A button got pushed
			//set the gpio's mode
			system("gpio mode ".$p." out");
			//set the gpio to high/low
			system("gpio write ".$p." ".$status );
			//echo("gpio write ".$p." ".$status );
			//reading pin's status
			unset ($status);
			exec ("gpio read ".$p, $status, $return );
			echo ( $status[0]  );
		} else {							// A sensor got queried
			//set the gpio's mode
			system("gpio mode ".$p." in");
			// take a reading
			unset ($status);
			exec ("gpio read ".$p, $status, $return );
			echo $status[0];
			//echo rand(1,100);  // that's mean
		}
		
	}
	else { echo ("fail"); }
} //print fail if cannot use values
else { echo ("fail"); }
?>
