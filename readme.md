# This is the Hydroponic and Irrigation app

The currently working and running version is PHP based. It is in the files *index.php* and *gpio.php*.

The new version is in python, and is in *server.py* and *helpers.py*. It is about 50% done. 
You'll need to install [flask](http://flask.pocoo.org/) to run it. 