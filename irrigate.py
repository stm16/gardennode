#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import datetime
import os

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

pinlist = [15,18]
for p in pinlist:
    GPIO.setup(p,GPIO.OUT)
    GPIO.output(p,0)

for x in range(100):
    for p in pinlist:
        GPIO.output(p,1)
	time.sleep(1.0)
	GPIO.output(p,0)

