<?

/* general interface to test pins on esp8266 12e.

*/


if (isset($_REQUEST['p'])) { // return pin status }
elseif (isset($_REQUEST['q'])) { // set a pin status }

else {

?>


var Pin = React.createClass {

        getInitialState: function() { return {label:'1', state:false, mode:'digital'}; },

        on: function() { this.setState({ state:true) }; },
        off: function() { this.setState( {state:false} ); },

        render: function() {
                return (
                        <span >GPIO {this.label}: {this.state} <button >Switch</button>
                )
        }       
} 


<?php

} // end of php logic

?>  
