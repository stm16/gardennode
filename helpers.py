import json
import RPi.GPIO as GPIO
import pdb
import yaml

# # # # # # # # # #
# # 
# # Python Helpers
# #

available_pins = [5,6,7]

def init_gpio():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.cleanup()
    
def check_all_pins():
    all = {}
    for p in available_pins:
        state = json.loads(check_pin(p))
        all[ state['pin'] ] = state['state']
    return json.dumps(all)
    
def check_pin(p):
    p = int(p)
    output = -1
    if p in available_pins:
        output = GPIO.input(p)
    return json.dumps( {'pin':p,'state':output} )

def set_pin(p,state):
    p = int(p)
    state = int(state)
    output = 'failed: bad pin or state'
    if p in available_pins and state in [1,0]:
        GPIO.setup(p,GPIO.OUT)
        GPIO.output(p,state)
        output = GPIO.input(p)
    return json.dumps( {'pin':p,'state':output} )
    
def pin_on(p):
    return set_pin(p,1)
def pin_off(p):
    return set_pin(p,0)

        

# # # # # # # # # #
# # 
# # The HTML
# #
#
# A button needs to display the right state (on/off)
# and make an ajax call when clicked, 
# and update it's image when it receives confirmation

# We're going to use jquery's .data() mechanism to store
#    - the pin number of the button
#    - the button's last known state

# Let's make the page expect a data structure like this:
#  [ {'pin':1,'status':0}, {'pin':2,'status':1},
#    {'pin':4,'status':22.5}, ]   
        
def tag(t,x): return "<"+t+">"+x+"</"+t+">"
def h1(x): return tag('h1',x)
def h2(x): return tag('h2',x)

def on_button(initial_label,pin_no=0):
    return "<button class='on_btn' id='obtn_"+str(pin_no)+"'>" + initial_label + "</button>"

def off_button(initial_label,pin_no=0):
    return "<button class='off_btn' id='fbtn_"+str(pin_no)+"'>" + initial_label + "</button>"

    
    
    
    
# # # # # # # # # #
# # 
# # Read the config file, do stuff, write the config file.
# #

def state_from_sched(sched):
    # given the schedule, and the current time,
    # what state should pins be in? 
    # Include any pins mentioned in schedule. 
    pass

def cron_read_and_do_config():
    # I get run every 15 seconds
    cmds = yaml.load(open('garden.yaml','r').read())
    #pdb.set_trace()
    print(cmds)

def web_from_config():
    cmds = yaml.load(open('garden.yaml','r').read())
    ret = my_js_eventhandler()
    for s in cmds['schedules']:
        ret += "Pin "
        ret += str(s['pin'])
        ret += " will turn "
        ret += str(s['newstate'])
        ret += " at "
        ret += str(s['time'])
        ret += "<br />\n"
    pins = [3,5,7]
    for p in pins:
        b = on_button(str("Turn on pin " + str(p)),str(p))
        c = off_button(str("Turn off pin " + str(p)),str(p))
        ret += c + " " + b + "<br />\n";
        
    
    return ret

if __name__ == '__main__':
    cron_read_and_do_config()


# # # # # # # # # #
# # 
# # The Javascript side of things
# #
# #
# #
# # 

def my_js_eventhandler():
    return """
<link rel="stylesheet" href="/static/style.css">
<script type='text/javascript' src='/static/jq331.js'></script>
<script type='text/javascript'>

// I go through the buttons, get their state, and 
// set up the appropriate event handlers
$(function() {
  // get all states and hide half the buttons
  // ...
  
  
  $('.on_btn').bind('click', function(evt) {
    console.log(evt);
    console.log(evt.target);
    var b_data = $(evt.target);
    var id = evt.target.id.toString().slice(5);
    var url = 'turnon/' + id;
    
    $('#fbtn_' + id).show();
    $(this).hide();
    console.log(url);
    $.getJSON(url,
        complete=function(data) {
            console.log('i received a response');
            console.log(data);
        });
    return false;
  });
  
    $('.off_btn').bind('click', function(evt) {
    console.log(evt);
    console.log(evt.target);
    var b_data = $(evt.target);
    var id = evt.target.id.toString().slice(5);
    var url = 'turnoff/' + id;
    
    $('#obtn_' + id).show();
    $(this).hide();
    console.log(url);
    $.getJSON(url,
        complete=function(data) {
            console.log('i received a response');
            console.log(data);
        });
    return false;
  });

  
});
</script>
"""






#
#  Junk I'm not using
#
#

"""
//button
<div class='container'>
    <h3>Test</h3>
        <form>
            <a href=# id=test><button class='btn btn-default'>Test</button></a>
        </form>

</div>"""






"""

# Lights demo
@app.route('/light/on/<buttonnum>')
def lighton(buttonnum):
    if pin_on(buttonnum):
        return 'I have turned on light number {}'.format(buttonnum)
    return 'You can change light number 5, 6, or 7.'
    
@app.route('/light/off/<buttonnum>')
def lightoff(buttonnum):
    if pin_off(buttonnum):
        return 'I have turned off light number {}'.format(buttonnum)
    return 'You can change light number 5, 6, or 7.'
    

"""
